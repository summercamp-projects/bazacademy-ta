from django.contrib.auth.models import User
from django.db import models
from django.db.models import DO_NOTHING, CASCADE


class Subject(models.Model):
    name = models.CharField()
    created_at = models.DateTimeField(auto_now_add=True)


class Problem(models.Model):
    title = models.CharField()
    description = models.TextField()
    input_description = models.TextField()
    output_description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    subject = models.ForeignKey(Subject, on_delete=DO_NOTHING)
    author = models.ForeignKey(User, on_delete=DO_NOTHING)


class TestCase(models.Model):
    input = models.TextField()
    output = models.TextField()
    is_sample = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)

    problem = models.ForeignKey(Problem, on_delete=CASCADE)


class Submission(models.Model):
    code = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(User, on_delete=DO_NOTHING)
    problem = models.ForeignKey(Problem, on_delete=DO_NOTHING)
